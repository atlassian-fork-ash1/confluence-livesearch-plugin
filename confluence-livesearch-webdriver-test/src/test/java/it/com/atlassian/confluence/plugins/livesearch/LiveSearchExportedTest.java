package it.com.atlassian.confluence.plugins.livesearch;

import com.atlassian.confluence.api.model.content.Content;
import com.atlassian.confluence.api.model.content.ContentRepresentation;
import com.atlassian.confluence.test.rpc.api.ConfluenceRpcClient;
import com.atlassian.confluence.test.stateless.ConfluenceStatelessTestRunner;
import com.atlassian.confluence.test.stateless.fixtures.Fixture;
import com.atlassian.confluence.test.stateless.fixtures.PageFixture;
import com.atlassian.confluence.test.stateless.fixtures.SpaceFixture;
import com.atlassian.confluence.test.stateless.fixtures.UserFixture;
import com.atlassian.confluence.webdriver.pageobjects.ConfluenceTestedProduct;
import com.atlassian.maven.plugins.unpacktests.ExportedTest;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import it.com.atlassian.confluence.plugins.livesearch.pageobjects.LiveSearchMacro;
import it.com.atlassian.confluence.plugins.livesearch.pageobjects.LiveSearchSearchResult;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import java.util.function.Predicate;

import static com.atlassian.confluence.test.rpc.api.permissions.SpacePermission.REGULAR_PERMISSIONS;
import static com.atlassian.confluence.test.stateless.fixtures.PageFixture.pageFixture;
import static com.atlassian.confluence.test.stateless.fixtures.SpaceFixture.spaceFixture;
import static com.atlassian.confluence.test.stateless.fixtures.UserFixture.userFixture;
import static java.lang.String.format;
import static java.util.stream.Collectors.toList;
import static java.util.stream.StreamSupport.stream;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

@ExportedTest
@RunWith(ConfluenceStatelessTestRunner.class)
public class LiveSearchExportedTest {

    @Inject
    private static ConfluenceTestedProduct product;
    @Inject
    private static ConfluenceRpcClient rpcClient;

    @Fixture
    private static UserFixture user = userFixture().build();
    @Fixture
    private static SpaceFixture space = spaceFixture()
            .permission(user, REGULAR_PERMISSIONS)
            .build();
    @Fixture
    private static PageFixture page = pageFixture()
            .space(space)
            .author(user)
            .title("Livesearch Test")
            .content("{livesearch}", ContentRepresentation.WIKI)
            .build();
    @Fixture
    private static PageFixture anotherPage = pageFixture()
            .space(space)
            .author(user)
            .title("Another Test Page")
            .build();

    private LiveSearchMacro getLiveSearchMacro(final Content page) {
        product.loginAndView(user.get(), page);
        rpcClient.getAdminSession().getSystemComponent().flushIndexQueue();
        return product.getPageBinder().bind(LiveSearchMacro.class);
    }

    @Test
    public void testLiveSearchWithGlobalScope() {
        final LiveSearchMacro macro = getLiveSearchMacro(page.get());

        final Iterable<LiveSearchSearchResult> results = macro.searchFor("test");

        //contains test page
        final Predicate<LiveSearchSearchResult> predicate = LiveSearchMacroTestUtil.searchResultByTitle(anotherPage.get().getTitle());
        Iterable<LiveSearchSearchResult> filtered = Iterables.filter(results, predicate::test);
        assertThat(Lists.newArrayList(filtered), hasSize(1));

        //contains test space
        final Predicate<LiveSearchSearchResult> spaceUrlPredicate = LiveSearchMacroTestUtil.searchResultByUrl(format("/display/%s", space.get().getKey()));
        final Predicate<LiveSearchSearchResult> contentTypePredicate = LiveSearchMacroTestUtil.searchResultByContentType("spacedesc");

        filtered = stream(results.spliterator(), false)
                .filter(spaceUrlPredicate.and(contentTypePredicate))
                .collect(toList());
        assertThat(Lists.newArrayList(filtered), hasSize(1));
    }
}
