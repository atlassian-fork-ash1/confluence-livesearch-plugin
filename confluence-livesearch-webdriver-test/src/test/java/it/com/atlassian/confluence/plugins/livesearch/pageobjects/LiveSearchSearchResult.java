package it.com.atlassian.confluence.plugins.livesearch.pageobjects;

public class LiveSearchSearchResult {
    private final String title;
    private final String additional;
    private final String url;
    private final String contentType;

    public String getTitle() {
        return title;
    }

    public String getAdditional() {
        return additional;
    }

    public String getUrl() {
        return url;
    }

    public String getContentType() {
        return contentType;
    }

    private LiveSearchSearchResult(Builder builder) {
        this.title = builder.title;
        this.additional = builder.additional;
        this.url = builder.url;
        this.contentType = builder.contentType;
    }

    public static Builder newSearchResult() {
        return new Builder();
    }


    public static class Builder {
        private String title;
        private String additional;
        private String url;
        private String contentType;

        public Builder setTitle(String title) {
            this.title = title;
            return this;
        }

        public Builder setAdditional(String additional) {
            this.additional = additional;
            return this;
        }

        public Builder setUrl(String url) {
            this.url = url;
            return this;
        }

        public Builder setContentType(String contentType) {
            this.contentType = contentType;
            return this;
        }

        public LiveSearchSearchResult build() {
            return new LiveSearchSearchResult(this);
        }
    }
}
